package com.ekar.project.assignment.services;

import com.ekar.project.assignment.config.threads.Consumer;
import com.ekar.project.assignment.config.threads.Producer;
import com.ekar.project.assignment.config.StoppableThread;
import com.ekar.project.assignment.utils.Counter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@Slf4j
@Service
@Scope("singleton")
public class ThreadPoolManagerImpl implements ThreadPoolManager {

    private final ThreadPoolExecutor producerExecutor;
    private final ThreadPoolExecutor consumerExecutor;

    private final Counter counter;

    //Have set a limit for threads. can be changed by replacing with newCachedThreadPool.
    public ThreadPoolManagerImpl(@Value("${thread.concurrent-thread-limit}") int threadLimit, Counter counter) {
        this.producerExecutor =  (ThreadPoolExecutor) Executors.newFixedThreadPool(threadLimit);
        this.consumerExecutor =  (ThreadPoolExecutor) Executors.newFixedThreadPool(threadLimit);
        this.counter = counter;
    }

    @Override
    public void addProducerThreads(int count) {
        while(count > 0) {
            producerExecutor.execute(new Producer(counter, producerExecutor.getTaskCount() + 1));
            --count;
        }
    }

    @Override
    public void addConsumerThreads(int count) {
        while(count > 0) {
            consumerExecutor.execute(new Consumer(counter, consumerExecutor.getTaskCount() + 1));
            --count;
        }
    }

    @Override
    public void stopAllThreads() {
//        producerExecutor.getQueue().forEach(x -> ((StoppableThread) x).stopThread());
        consumerExecutor.getQueue().forEach(x -> ((StoppableThread) x).stopThread());

        producerExecutor.shutdownNow();
        consumerExecutor.shutdownNow();
    }
}
