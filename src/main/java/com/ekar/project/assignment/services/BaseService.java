package com.ekar.project.assignment.services;

import com.ekar.project.assignment.dto.BaseDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public abstract class BaseService<E, D extends BaseDTO, R extends JpaRepository<E, Integer>> {

    protected final R repository;

    public BaseService(R repository) {
        this.repository = repository;
    }

    public D findById(int id) {
        return repository.findById(id).map(this::mapEntityToDto).orElse(null);
    }

    public List<D> findAll() {
        return repository.findAll()
                .stream()
                .map(this::mapEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public D save(D dto) {
        var e = mapDtoToEntity(dto);
        e = repository.save(e);
        return mapEntityToDto(e);
    }

    public E getOne(int id) {
        return repository.getOne(id);
    }

    public E findOne(int id) {
        return repository.findById(id).orElse(null);
    }

    public D disable(int id) {
        D obj = findById(id);
        obj.setEnable(false);
        obj = this.save(obj);
        return obj;
    }

    public abstract D mapEntityToDto(E entity);

    public abstract E mapDtoToEntity(D dto);
}
