package com.ekar.project.assignment.services;

public interface ThreadPoolManager {

    void addProducerThreads(int count);

    void addConsumerThreads(int count);

    void stopAllThreads();
}
