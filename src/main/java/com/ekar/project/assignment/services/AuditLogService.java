package com.ekar.project.assignment.services;

import com.ekar.project.assignment.dto.AuditLogDTO;
import com.ekar.project.assignment.dto.BaseDTO;
import com.ekar.project.assignment.model.AuditLog;
import com.ekar.project.assignment.repository.AuditLogRepository;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuditLogService extends BaseService<AuditLog, AuditLogDTO, AuditLogRepository> {

    public AuditLogService(AuditLogRepository repository) {
        super(repository);
    }

    @Override
    public AuditLogDTO mapEntityToDto(AuditLog entity) {
        AuditLogDTO dto = new AuditLogDTO();

        dto.setPreviousPayload(entity.getPreviousPayload());
        dto.setCurrentPayload(entity.getCurrentPayload());
        dto.setUserId(entity.getUserId());
        dto.setDocumentNo(entity.getDocumentNo());
        dto.setApi(entity.getApi());
        dto.setAction(entity.getAction());
        dto.setEnable(entity.isEnable());
        dto.setCreatedDate(entity.getCreatedDate().getTime());

        return dto;
    }

    @Override
    public AuditLog mapDtoToEntity(AuditLogDTO dto) {
        AuditLog entity = repository.findById(dto.getId()).orElse(null);

        if (null == entity) {
            entity = new AuditLog();
            entity.setCreatedDate(new Date());
        }

        entity.setPreviousPayload(dto.getPreviousPayload());
        entity.setCurrentPayload(dto.getCurrentPayload());
        entity.setUserId(dto.getUserId());
        entity.setDocumentNo(dto.getDocumentNo());
        entity.setApi(dto.getApi());
        entity.setAction(dto.getAction());
        entity.setEnable(dto.isEnable());

        return entity;
    }

    public List<AuditLogDTO> findAllEnabled() {
        return repository.findAllEnabled()
                .stream()
                .map(this::mapEntityToDto)
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public <T extends BaseDTO> void processAuditLog(T dto, String api) {
        AuditLog auditLog = new AuditLog();

        auditLog.setCreatedDate(new Date());
        auditLog.setAction(dto.getId() == 0 ? "Create" : dto.isEnable() ? "Update" : "Delete");
        auditLog.setApi(api);
        auditLog.setDocumentNo(dto.getId());
        auditLog.setCurrentPayload(dto.toString());
        auditLog.setPreviousPayload("");
        auditLog.setEnable(true);
        auditLog.setUserId(1); //setting this as direct value for now, added this thinking there would be security

        repository.save(auditLog);
    }
}
