package com.ekar.project.assignment.services;

import com.ekar.project.assignment.dto.BaseDTO;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

@Slf4j
public abstract class BaseAuditService<E, D extends BaseDTO, R extends JpaRepository<E, Integer>> extends BaseService<E, D, R> {

    private final AuditLogService auditLogService;
    private final Class<E> entityClass;

    public BaseAuditService(R repository, AuditLogService auditLogService) {
        super(repository);
        this.auditLogService = auditLogService;

        Type t = getClass().getGenericSuperclass();
        Type arg;
        if (t instanceof ParameterizedType) {
            arg = ((ParameterizedType) t).getActualTypeArguments()[0];
        } else if (t instanceof Class) {
            arg = ((ParameterizedType) ((Class<?>) t).getGenericSuperclass()).getActualTypeArguments()[0];

        } else {
            log.error("Can not handle type construction for '" + getClass() + "'!");
            throw new RuntimeException("Can not handle type construction for '" + getClass() + "'!");
        }

        if (arg instanceof Class) {
            this.entityClass = (Class<E>) arg;
        } else if (arg instanceof ParameterizedType) {
            this.entityClass = (Class<E>) ((ParameterizedType) arg).getRawType();
        } else {
            log.error("Problem dtermining generic class for '" + getClass() + "'! ");

            throw new RuntimeException("Problem dtermining generic class for '" + getClass() + "'! ");
        }
    }

    protected Class<E> getEntityClass() {
        return this.entityClass;
    }

    @SneakyThrows
    @Override
    public D save(D dto) {
        auditLogService.processAuditLog(dto, getEntityClass().getSimpleName());
        dto = super.save(dto);
        log.info(dto.toString());
        return dto;
    }
}
