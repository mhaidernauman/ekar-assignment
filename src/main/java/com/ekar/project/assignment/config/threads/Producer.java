package com.ekar.project.assignment.config.threads;

import com.ekar.project.assignment.config.StoppableThread;
import com.ekar.project.assignment.utils.Counter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Producer implements StoppableThread {

    private final Counter counter;
    private final long producerId;

    private boolean breakLoop = false;

    public Producer(Counter counter, long producerId) {
        this.counter = counter;
        this.producerId = producerId;
    }

    @Override
    public void run() {

        while(!breakLoop) {
            var counterVal = counter.addToCounter();
            log.info("producer-" + producerId + " produced with counter value: " + counterVal);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.error(e.getMessage(), e);
                break;
            }
        }
    }

    @Override
    public void stopThread() {
        log.info("Thread Stop called for producer-" + producerId);
        breakLoop = true;
    }
}
