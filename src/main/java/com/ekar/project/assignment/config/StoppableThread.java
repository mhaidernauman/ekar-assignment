package com.ekar.project.assignment.config;

public interface StoppableThread extends Runnable {

    void stopThread();
}
