package com.ekar.project.assignment.config.threads;

import com.ekar.project.assignment.config.StoppableThread;
import com.ekar.project.assignment.utils.Counter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Consumer implements StoppableThread {

    private final Counter counter;
    private final long consumerId;

    private boolean breakLoop = false;

    public Consumer(Counter counter, long consumerId) {
        this.counter = counter;
        this.consumerId = consumerId;
    }

    @Override
    public void run() {

        while(!breakLoop) {
            var counterVal = counter.subtractFromCounter();
            log.info("consumer-" + consumerId + " consumed with counter value: " + counterVal);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.error(e.getMessage(), e);
                break;
            }
        }
    }

    @Override
    public void stopThread() {
        log.info("Thread Stop called for consumer-" + consumerId);
        breakLoop = true;
    }
}
