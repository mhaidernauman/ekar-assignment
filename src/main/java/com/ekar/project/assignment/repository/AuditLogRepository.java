package com.ekar.project.assignment.repository;

import com.ekar.project.assignment.model.AuditLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AuditLogRepository extends JpaRepository<AuditLog, Integer> {

    @Query("SELECT al FROM AuditLog al where al.enable = true")
    List<AuditLog> findAllEnabled();
}
