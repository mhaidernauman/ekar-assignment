package com.ekar.project.assignment.controller;

import com.ekar.project.assignment.dto.AuditLogDTO;
import com.ekar.project.assignment.services.AuditLogService;
import com.ekar.project.assignment.services.ThreadPoolManager;
import com.ekar.project.assignment.utils.Counter;
import com.ekar.project.assignment.utils.CounterLimitListener;
import com.ekar.project.assignment.utils.LimitType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ThreadController {

    private final AuditLogService auditLogService;
    private final ThreadPoolManager threadPoolManager;
    private final Counter counter;

    public ThreadController(AuditLogService auditLogService, ThreadPoolManager threadPoolManager, Counter counter) {
        this.auditLogService = auditLogService;
        this.threadPoolManager = threadPoolManager;
        this.counter = counter;

        this.threadPoolManager.addProducerThreads(3);
        this.threadPoolManager.addConsumerThreads(3);

        this.counter.setCounterLimitListener(new CounterLimitListener() {
            @Override
            public void listen(LimitType limitType, int counter) {
                var auditLogDto = new AuditLogDTO();

                auditLogDto.setApi("null");
                auditLogDto.setAction("terminating threads");
                auditLogDto.setCurrentPayload(limitType.toString());
                auditLogDto.setPreviousPayload(limitType.toString());
                auditLogDto.setUserId(0);
                auditLogDto.setEnable(true);
                auditLogDto.setDocumentNo(counter);

                auditLogService.save(auditLogDto);

                threadPoolManager.stopAllThreads();
            }
        });
    }

    @PostMapping("create")
    public ResponseEntity<String> addProducerConsuer(@RequestParam(value = "producer") int producerCount,
                                                     @RequestParam(value = "consumer") int consumerCount) {
        threadPoolManager.addProducerThreads(producerCount);
        threadPoolManager.addConsumerThreads(consumerCount);

        var auditLogDto = new AuditLogDTO();

        auditLogDto.setApi("null");
        auditLogDto.setAction("Adding producers and consumers");
        auditLogDto.setCurrentPayload("{producer:" + producerCount + ", consumer: " + consumerCount + "}");
        auditLogDto.setPreviousPayload("{producer:" + producerCount + ", consumer: " + consumerCount + "}");
        auditLogDto.setUserId(0);
        auditLogDto.setEnable(true);
        auditLogDto.setDocumentNo(0);

        auditLogService.save(auditLogDto);

        return ResponseEntity.status(HttpStatus.CREATED).body("created");
    }

    @PostMapping("update-counter/{counterValue}")
    public ResponseEntity<String> updateCounter(@PathVariable("counterValue") int counterValue) {
        counter.updateCounter(counterValue);
        return ResponseEntity.status(HttpStatus.OK).body("Updated");
    }
}
