package com.ekar.project.assignment.utils;

public interface Counter {

    int addToCounter();

    int subtractFromCounter();

    int getCounterValue();

    void updateCounter(int counterValue);

    void setCounterLimitListener(CounterLimitListener limitListener);
}
