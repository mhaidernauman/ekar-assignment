package com.ekar.project.assignment.utils;

public interface CounterLimitListener {

    void listen(LimitType limitType, int counterValue);
}
