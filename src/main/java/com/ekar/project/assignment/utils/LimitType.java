package com.ekar.project.assignment.utils;

public enum LimitType {
    UPPER_BOUND,
    LOWER_BOUND;
}
