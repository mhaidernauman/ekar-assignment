package com.ekar.project.assignment.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.locks.ReentrantLock;

@Slf4j
@Component
@Scope("singleton")
public class CounterImpl implements Counter {

    private int counter;
    private final ReentrantLock lock = new ReentrantLock();

    private CounterLimitListener limitListener;

    private int upperBound;
    private int lowerBound;

    public CounterImpl(@Value("${counter.start-value}") int counter,
                       @Value("${counter.upper-bound}") int upperBound,
                       @Value("${counter.lower-bound}") int lowerBound) {
        this.counter = counter;
        this.upperBound = upperBound;
        this.lowerBound = lowerBound;

        log.info("Counter initialized with default value : " + counter);
        log.info("Counter initialized with Upper Limit : " + upperBound);
        log.info("Counter initialized with Lower Limit : " + lowerBound);
    }

    @Override
    public int addToCounter() {
        lock.lock();
        var val = ++counter;
        verifyLimit();
        lock.unlock();
        return val;
    }

    @Override
    public int subtractFromCounter() {
        lock.lock();
        var val = --counter;
        lock.unlock();
        verifyLimit();
        return val;
    }

    @Override
    public int getCounterValue() {
        lock.lock();
        var val = counter;
        verifyLimit();
        lock.unlock();
        return val;
    }

    @Override
    public void updateCounter(int counterValue) {
        lock.lock();
        counter = counterValue;
        verifyLimit();
        lock.unlock();
    }

    @Override
    public void setCounterLimitListener(CounterLimitListener limitListener) {
        this.limitListener = limitListener;
    }

    private void verifyLimit() {
        if(null == limitListener)
            return;

        if(counter <= lowerBound)
            limitListener.listen(LimitType.LOWER_BOUND, counter);
        else if(counter >= upperBound)
            limitListener.listen(LimitType.UPPER_BOUND, counter);
    }
}
