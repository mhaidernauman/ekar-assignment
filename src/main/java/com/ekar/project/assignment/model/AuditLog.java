package com.ekar.project.assignment.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "audit_log")
public class AuditLog extends BaseEntity {

    private String api;

    private int documentNo;

    private String previousPayload;

    private String currentPayload;

    private String action;

    private int userId;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date createdDate;
}
