package com.ekar.project.assignment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

//    @Temporal(TemporalType.TIMESTAMP)
//    private Date modifiedDate;

//    private int createdBy;

//    private int modifiedBy;

    private boolean enable;

}
