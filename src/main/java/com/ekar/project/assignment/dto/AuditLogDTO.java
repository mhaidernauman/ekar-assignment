package com.ekar.project.assignment.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class AuditLogDTO extends BaseDTO {

    private String api;

    private int documentNo;

    private String previousPayload;

    private String currentPayload;

    private String action;

    private int userId;

    private long createdDate;

    private long modifiedDate;
}