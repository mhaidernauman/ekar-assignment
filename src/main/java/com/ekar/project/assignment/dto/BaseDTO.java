package com.ekar.project.assignment.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public abstract class BaseDTO {

    private int id;

    @NotNull(message = "Record enable can not be null")
    private boolean enable;
}
