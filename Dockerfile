FROM adoptopenjdk/openjdk11:ppc64le-ubuntu-jre-11.0.10_9
COPY target/assignment-0.0.1-SNAPSHOT.jar application.jar
ENTRYPOINT ["java","-jar","application.jar"]